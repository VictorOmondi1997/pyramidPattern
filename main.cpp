#include <iostream>

using namespace std;

int main()
{
    int count;
    cout<< "Enter The Number of Lines The Pyramid Should Occupy: \n";
    cin>>count;
    for(int i=1; i<=count*2; i++){
        if(i%2 != 0){
            for(int j=1; j<=i; j++){
                cout<< "*";
            }
        }else{
            continue;
        }
        cout<< endl;
    }

    for(int i=count*2; i>=0; i--){
        if(i%2 == 0){
            for(int j=0; j<=i; j++){
                cout<< "#";
            }
        }else{
            continue;
        }
        cout<< endl;
    }

    cout<< "\n\n This is a Parallelogram: \n";
    for(int i=count; i>=0; i--){
        for(int j=0; j<=i; j++)
            cout<< " ";
            for(int k=1; k<=count*2; k++){
                if(k%2 != 0){
                    for(int l=1; l<=k; l++)
                        cout<< "*";
                    }else{
                        continue;
                    }
            }
        cout<< endl;
    }
    return 0;
}
